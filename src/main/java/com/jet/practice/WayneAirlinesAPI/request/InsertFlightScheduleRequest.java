package com.jet.practice.WayneAirlinesAPI.request;

import java.util.Date;

import org.joda.time.DateTime;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "InsertFlightScheduleRequest", description = "insert flight schedule Request")
public class InsertFlightScheduleRequest {

	private String flightScheduleCode;
	private String originAirportCode;
	private String destinationAirportCode;
	private Date flightDateStart;
	private Date flightDateFinish;
	private String carrierCode;
	private Integer currentCapacity;
	private String flightType;
	private String status;

	
	public String getFlightScheduleCode() {
		return flightScheduleCode;
	}

	public void setFlightScheduleCode(String flightScheduleCode) {
		this.flightScheduleCode = flightScheduleCode;
	}

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public Date getFlightDateStart() {
		return flightDateStart;
	}

	public void setFlightDateStart(Date flightDateStart) {
		this.flightDateStart = flightDateStart;
	}

	public Date getFlightDateFinish() {
		return flightDateFinish;
	}

	public void setFlightDateFinish(Date flightDateFinish) {
		this.flightDateFinish = flightDateFinish;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Integer getCurrentCapacity() {
		return currentCapacity;
	}

	public void setCurrentCapacity(Integer currentCapacity) {
		this.currentCapacity = currentCapacity;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InsertFlightScheduleRequest [flightScheduleCode=" + flightScheduleCode + ", originAirportCode="
				+ originAirportCode + ", destinationAirportCode=" + destinationAirportCode + ", flightDateStart="
				+ flightDateStart + ", flightDateFinish=" + flightDateFinish + ", carrierCode=" + carrierCode
				+ ", currentCapacity=" + currentCapacity + ", flightType=" + flightType + ", status=" + status + "]";
	}

	

}
