package com.jet.practice.WayneAirlinesAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WayneAirlinesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WayneAirlinesApiApplication.class, args);
	}
}
