package com.jet.practice.WayneAirlinesAPI.service;

import com.jet.practice.WayneAirlinesAPI.entity.Carrier;

public interface CarrierService {
	
	Carrier getCarrierByCode(String code);

}
