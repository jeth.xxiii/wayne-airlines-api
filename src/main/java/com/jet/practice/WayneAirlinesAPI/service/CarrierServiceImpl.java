package com.jet.practice.WayneAirlinesAPI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.Carrier;
import com.jet.practice.WayneAirlinesAPI.mapper.CarrierMapper;

@Service
@Transactional
public class CarrierServiceImpl implements CarrierService {
	
	@Autowired
	private CarrierMapper carrierMapper;

	@Override
	public Carrier getCarrierByCode(String code) {
		return carrierMapper.getCarrierByCode(code);
	}

}
