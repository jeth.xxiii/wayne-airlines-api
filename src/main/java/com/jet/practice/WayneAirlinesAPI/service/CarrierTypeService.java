package com.jet.practice.WayneAirlinesAPI.service;

import com.jet.practice.WayneAirlinesAPI.entity.CarrierType;

public interface CarrierTypeService {
	
	CarrierType getCarrierTypeByCode(String code);

}
