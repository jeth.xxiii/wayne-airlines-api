package com.jet.practice.WayneAirlinesAPI.mapper;

import com.jet.practice.WayneAirlinesAPI.entity.User;

public interface UserMapper {

	User getUserByUsername(String username);

}
