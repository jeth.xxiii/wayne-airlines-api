package com.jet.practice.WayneAirlinesAPI.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jet.practice.WayneAirlinesAPI.entity.Carrier;
import com.jet.practice.WayneAirlinesAPI.request.GenericGetCodeRequest;
import com.jet.practice.WayneAirlinesAPI.service.CarrierService;

@RestController
@RequestMapping("/api")
public class CarrierEndpoint {
	
	@Autowired
	private CarrierService carrierService;
	
	@PostMapping(value="/getCarrierByCode", produces="application/json")
	private Carrier getCarrierByCode(@RequestBody GenericGetCodeRequest request) {
		return carrierService.getCarrierByCode(request.getCode());
		
	}

}
