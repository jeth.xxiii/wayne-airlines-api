package com.jet.practice.WayneAirlinesAPI.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jet.practice.WayneAirlinesAPI.entity.User;
import com.jet.practice.WayneAirlinesAPI.service.UserService;

@RestController
@RequestMapping("/api/login")
public class LoginEndpoint {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET, produces = "application/json")
	public User getUser(@PathVariable String username) {
		return userService.getUserByUsername(username);
	}

}
