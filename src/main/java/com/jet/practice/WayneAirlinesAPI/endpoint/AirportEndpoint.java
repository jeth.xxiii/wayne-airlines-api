package com.jet.practice.WayneAirlinesAPI.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;
import com.jet.practice.WayneAirlinesAPI.request.GenericGetCodeRequest;
import com.jet.practice.WayneAirlinesAPI.request.InsertAirportRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;
import com.jet.practice.WayneAirlinesAPI.service.AirportService;

@RestController
@RequestMapping("/api")
public class AirportEndpoint {
	
	@Autowired
	private AirportService airportService;
	
	@RequestMapping(value = "/airports/", method = RequestMethod.GET, produces = "application/json")
	private List<Airport> getAllAirportByCountry(){
		return airportService.getAllAirportsByCountry();
	}
	
	@PostMapping(value="/getAiportByCode", produces="application/json")
	private Airport getAiportByCode(@RequestBody GenericGetCodeRequest request) {
		return airportService.getAirportByCode(request.getCode());
		
	}
	
	@PostMapping(value="/getAiportByCountryCode", produces="application/json")
	private List<Airport> getAiportByCountryCode(@RequestBody GenericGetCodeRequest request) {
		return airportService.getAirportByCountryCode(request.getCode());
		
	}
	
	@PostMapping(value="/createAirport", produces="application/json")
	private GenericCodeResponse createAirport(@RequestBody InsertAirportRequest request) {
		return airportService.createAirport(request);
	}
	

}
