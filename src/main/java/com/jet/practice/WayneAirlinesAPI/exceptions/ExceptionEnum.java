package com.jet.practice.WayneAirlinesAPI.exceptions;


public enum ExceptionEnum {

	INTERNAL_SERVER_ERROR(101,"Internal server error"),
	
	MISSING_PARAMETER_ERROR(102,"Required parameter/s missing"),
	
	COUNTRY_NOT_EXISTING_ERROR(103,"Country does not exist"),
	
	AIRPORT_ALREADY_EXISTING_ERROR(104,"Airport already exists"),
	
	FLIGHT_SCHEDULE_EXISTING_ERROR(105, "Flight schedule already existing"),
	
	AIRPORT_DOES_NOT_EXIST_ERROR(106, "Airport not existing"),
	
	CARRIER_DOES_NOT_EXIST_ERROR(107, "Carrier not existing"),
	
	INVALID_FLIGHT_DATE_START_ERROR(108,"Departure date is invalid or date is after arrival date"),
	
	INVALID_FLIGHT_DATE_FINISH_ERROR(109,"Arrival date is invalid or date is before depature date "),
	
	CONFLICTING_FLIGHT_START_ERROR(110, "Depature date conflicts with same carrier and airport in the schedule"),
	
	CONFLICTING_FLIGHT_FINISH_ERROR(111, "Arrival date conflicts with same carrier and airport in the schedule"),
	
	FLIGHT_NOT_FINISH_ERROR(112, "Flight is still in transit"),
	
	FLIGHT_SCHEDULE_INTERVAL_ERROR(113, "Flight schedule interval is less than 2 hours, give thep pilots a break!"),
	
	FLIGHT_OVER_CAPACITY_ERROR(114, "Over capacity!");
	
	private Integer code;
	private String message;
	
	ExceptionEnum(Integer code,String message){
		
		this.code = code;
		this.message = message;
	}
	
	public Integer code() {
		
		return this.code;
	}
	public String message() {
		
		return this.message;
	}
}
