package com.jet.practice.WayneAirlinesAPI.entity;

public class Airport {

	private String code;
	private String name;
	private Country country;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Airport [code=" + code + ", name=" + name + ", country=" + country + "]";
	}

}
