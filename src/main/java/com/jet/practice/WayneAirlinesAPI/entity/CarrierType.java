package com.jet.practice.WayneAirlinesAPI.entity;

public class CarrierType {

	private Integer id;
	private String name;
	private String code;
	private Integer capacity;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CarrierType [id=" + id + ", name=" + name + ", code=" + code + ", capacity=" + capacity
				+ ", description=" + description + "]";
	}

}
