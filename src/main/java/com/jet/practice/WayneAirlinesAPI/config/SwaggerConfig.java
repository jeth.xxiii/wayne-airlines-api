package com.jet.practice.WayneAirlinesAPI.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${swagger.package.path:com.jet.practice.WayneAirlinesAPI.endpoint}")
	private String packagePath;
	@Value("${swagger.title:Wayne-API}")
	private String title;
	@Value("${swagger.desc:A personal project by Jethro Torres}")
	private String desc;
	@Value("${swagger.contant:Jet}")
	private String contant;
	@Value("${swagger.email:jeth.xxiii@gmail.com}")
	private String email;
	@Value("${swagger.version:1.0}")
	private String version;
	
	@Value("${spring.mvc.prop.snake.case:true}")
	private String propName;
	

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage(packagePath)).paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(title).description(desc).contact(new Contact(contant, "", email))
				.version(version).build();
	}

}
